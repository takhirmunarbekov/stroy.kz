angular.module('decode')
    .controller('MainBodyCtrl', MainBodyCtrl);

MainBodyCtrl.$inject = ['$http','$state','$rootScope','$location','$cookies'];

function MainBodyCtrl($http,$state,$rootScope,location,$cookies){
    var vm = this;
    vm.showModal=false;
    vm.showUser=false;
    vm.showHeaderValue = true;

    console.log($rootScope)
    vm.login = function(){
        $http.post('/api/login', {
            email: vm.mail,
            password: vm.password
        })
            .success(function(response){
                $cookies.putObject('user',response);
                $rootScope.session = true;
                $rootScope.user_id = $cookies.getObject('user')._id;
                console.log(response);
                $state.go('cabinet')
            })
            .error(function(err){
                console.log(err);
            });
    };

    if($cookies.get('user')){
        $rootScope.session=true;
        $rootScope.user = $cookies.getObject('user');
        console.log($rootScope.user)
    }else{
        $rootScope.session=false;
    }


    vm.logout=function(){
        $http.post('api/logout')
            .success(function(response){
                $rootScope.session = false;
                $cookies.putObject('user');
            })
            .error(function(err){
                console.log(err)
            })
    };

    vm.addMaterial = function(){
        if($rootScope.user){
            $state.go('cabinet')
        }else{
            $state.go('add_material')
        }
    }
    //jQuery
    // ===== Scroll to Top ====
    
    // $(window).scroll(function() {
    //     if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
    //         $('.arrow-up').fadeIn(200);    // Fade in the arrow
    //     } else {
    //         $('.arrow-up').fadeOut(200);   // Else fade out the arrow
    //     }
    // });
    // $('.arrow-up').click(function() {      // When arrow is clicked
    //     $('body,html').animate({
    //         scrollTop : 0                       // Scroll to top of body
    //     }, 500);
    // });

}