angular.module('decode')
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])
    .controller('AdminCtrl', AdminCtrl);

AdminCtrl.$inject = ['$http','$state'];

function AdminCtrl($http,$state){
    var vm = this;
    vm.posts=[];
    vm.users=[];
    vm.AdminValue='';
    vm.GenderValue='';

    vm.createPost = function(){
        var data = new FormData();

        data.append("name",vm.name);
        data.append("type",vm.type);
        data.append("cost",vm.cost);
        data.append("company",vm.company);
        data.append("rating",vm.rating);
        data.append("gender",vm.gender);
        data.append("sale",vm.sale+"%");
        data.append("new",vm.new);
        data.append("image",vm.file);

        const option = {
            transformRequest: angular.identity,
            headers:{"Content-Type":undefined}
        };


        $http.post('/api/post', data, option)
            .success(function(response){
                console.log(response);
                vm.posts.push(response);
            })
            .error(function(err){
                console.log(err);
            });
    };

    vm.removePost = function(id,index){

        $http.delete('/api/delete/'+id)
            .success(function(response){
                vm.posts.splice(index,1);
            })
            .error(function(err){
                console.log(err);
            });
    };

    
    
    $http.get('/api/posts')
        .success(function(response){
            vm.posts = response;
            console.log(vm.posts);
        })
        .error(function(err){
            alert(err);
        });

    vm.updatePost=function(){
        $http.put('/api/posts',vm.currentPost)
            .success(function(response)
            {
                alert('Success');
            });
    }
        

    $http.get('/api/users')
        .success(function(response){
            vm.users = response;
            console.log(vm.users);
        })
        .error(function(err){

        });
}