angular.module('decode')
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])
    .controller('CabinetCtrl', CabinetCtrl);

CabinetCtrl.$inject = ['$http','$state','$cookies','$rootScope'];

function CabinetCtrl($http,$state,$cookies,$rootScope){
    var vm = this;
    vm.posts=[];
    vm.users=[];
    vm.cabinetValue = 1;

    vm.type = [
            ['Внешняя отделка',[
                ['Строительные смеси',
                    ['Наливные полы стяжки ','Сухие смеси','Шпаклевки','Штукатурки']
                ],
                ['Лакокрасочные материалы',
                    ['Водно-дисперсионная краска',
                        'Готовые грунтовки',
                    ' Декоративные покрытия',
                        'Краски специального назначения',
                        'Лаки',
                        'Олифа',
                        'Покрытие для камня, кирпича, бетона',
                        'Средства для защиты древесины',
                        'Эмали'
                        ]
                ],
                ['Строительная Химия',
                    [
                        'Герметики',
                        'Гидроизоляция строительных конструкций',
                        'Добавки',
                        'Жидкие Гвозди',
                        'Монтажная Пена',
                        'Очистители',
                        'Средства для ухода за напольными покрытиям'
                    ]
                ]
            ]
        ],
        ['Внутренняя отделка'],
        ['Инструменты']
    ]
    console.log(vm.type[0][0])

    
    vm.showHello = function(a){
        console.log(a)
    }




    vm.createPost = function(){
        var data = new FormData();

        data.append("name",vm.name);
        data.append("type",vm.type);
        data.append("cost",vm.cost);
        data.append("company",vm.company);
        data.append("rating",vm.rating);
        data.append("gender",vm.gender);
        data.append("sale",vm.sale+"%");
        data.append("new",vm.new);
        data.append("image",vm.file);

        const option = {
            transformRequest: angular.identity,
            headers:{"Content-Type":undefined}
        };


        $http.post('/api/post', data, option)
            .success(function(response){
                console.log(response);
                vm.posts.push(response);
            })
            .error(function(err){
                console.log(err);
            });
    };

    vm.removePost = function(id,index){

        $http.delete('/api/delete/'+id)
            .success(function(response){
                vm.posts.splice(index,1);
            })
            .error(function(err){
                console.log(err);
            });
    };

    
    
    $http.get('/api/posts')
        .success(function(response){
            vm.posts = response;
            console.log(vm.posts);
        })
        .error(function(err){
            alert(err);
        });

    vm.updatePost=function(){
        $http.put('/api/posts',vm.currentPost)
            .success(function(response)
            {
                alert('Success');
            });
    }
        

    $http.get('/api/users')
        .success(function(response){
            vm.users = response;
            console.log(vm.users);
        })
        .error(function(err){

        });
    
    
    

    if($cookies.get('user')){
        $rootScope.session=true;
        $rootScope.user = $cookies.getObject('user');
        vm.cabinetValue = $cookies.getObject('cabinet')
        if($rootScope.user.role == 'company'){
            console.log($rootScope.user.role)
        }else{
            
        }
    }else{
        $rootScope.session=false;
        $state.go('registration')
    }
    
    vm.cabinet = function(n){
        $cookies.putObject('cabinet',n);
        vm.cabinetValue = n
    }


}