angular.module('decode')
    .controller('signUpCtrl', signUpCtrl);

signUpCtrl.$inject = ['$http','$state','$rootScope',"$cookies","$window"];

function signUpCtrl($http,$state,$rootScope,$cookies,$window){

    var vm = this;
    vm.register = function(){
        $http.post('/api/signup', {
            name: vm.name,
            email: vm.email,
            password: vm.password
        })
            .success(function(response){
            })
            .error(function(err){
                console.log(err);
            });
    };
    
    vm.login = function(){
        $http.post('/api/login', {
            email: vm.email,
            password: vm.password
        })
            .success(function(response){
                $cookies.putObject('user',response);
                $rootScope.session = true;
                $rootScope.user_id = $cookies.getObject('user')._id;
                console.log("welcome");
                $window.location.href = '/home';
            })
            .error(function(err){
                console.log(err);
            });
    };

    if($cookies.get('user')){
        $rootScope.session=true;
        $rootScope.user_id = $cookies.getObject('user')._id;
    }else{
        $rootScope.session=false;
    }
}