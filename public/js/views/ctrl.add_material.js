angular.module('decode')
    .controller('AddMaterialCtrl', AddMaterialCtrl);
    
AddMaterialCtrl.$inject = ['$http','$state','$rootScope',"$cookies","$window"];

function AddMaterialCtrl($http,$state,$rootScope,$cookies,$window){
    
    var vm = this;

    vm.form = 1
    vm.showModal = false

    vm.signUp = function(){
        if(vm.password == vm.confirm_password){
            $http.post('/api/signup', {
                name: vm.name,
                email: vm.mail,
                password: vm.password,
                phone: vm.phone,
                company_type: vm.company_type,
                company_name: vm.company_name,
                company_mail: vm.company_mail,
                company_phone: vm.company_phone,
                company_location: vm.company_location,
                role:'company'
            })
                .success(function(response){
                })
                .error(function(err){
                    console.log(err);
                });
        }
    };


    // vm.login = function(){
    //     $http.post('/api/login', {
    //         email: vm.email,
    //         password: vm.password
    //     })
    //         .success(function(response){
    //             $cookies.putObject('user',response);
    //             $rootScope.session = true;
    //             $rootScope.user_id = $cookies.getObject('user')._id;
    //             console.log("welcome");
    //             $window.location.href = '/home';
    //         })
    //         .error(function(err){
    //             console.log(err);
    //         });
    // };

    if($cookies.get('user')){
        $rootScope.session=true;
        $rootScope.user_id = $cookies.getObject('user')._id;
    }else{
        $rootScope.session=false;
    }
}