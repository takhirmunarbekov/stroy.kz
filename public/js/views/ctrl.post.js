angular.module('decode')
    .controller('PostCtrl', PostCtrl);

PostCtrl.$inject = ['$http','$state','$rootScope',"$cookies"];

function PostCtrl($http,$state,$rootScope,$cookies){
    var vm = this;
    //jQuery
    vm.id=$state.params.id;
    $http.get('api/post/'+vm.id)
        .success(function(post){
            vm.post=post;
            console.log(vm.post);
        })
        .error(function(err){
            $state.go('home');
        });

    $http.get('/api/profile/'+$state.params.id)
        .success(function(profile){
            vm.profile=profile;
        })
        .error(function(err){
            $state.go('home');
        });
}