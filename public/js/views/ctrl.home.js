angular.module('decode')
    .controller('HomeCtrl', HomeCtrl);

HomeCtrl.$inject = ['$http','$state'];

function HomeCtrl($http,$state){
    var vm = this;
    //jQuery
    $http.get('/api/posts')
        .success(function(response){
            vm.posts = response;
            console.log(vm.posts);
        })
        .error(function(err){
            alert(err);
        });

    vm.updatePost=function(){
        $http.put('/api/posts',vm.currentPost)
            .success(function(response)
            {
                alert('Success');
            });
    }
}