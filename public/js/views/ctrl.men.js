angular.module('decode')
    .controller('menCtrl', menCtrl);

menCtrl.$inject = ['$http','$state'];

function menCtrl($http,$state){
    var vm = this;

    $http.get('/api/posts')
        .success(function(response){
            vm.posts = response;
            console.log(vm.posts);
        })
        .error(function(err){
            alert(err);
        });

    vm.updatePost=function(){
        $http.put('/api/posts',vm.currentPost)
            .success(function(response)
            {
                alert('Success');
            });
    };
}