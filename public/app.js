angular.module('decode',
    ['ui.router',
    'ngCookies'])
    .config(routeConfig);

    routeConfig.$inject=['$stateProvider','$locationProvider','$urlRouterProvider'];

    function routeConfig($stateProvider,$locationProvider,$urlRouterProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('startPage', {
                url: '/start',
                templateUrl: '/views/start.html',
                controller: 'StartCtrl',
                controllerAs: 'vm'
            })
            .state('home', {
                url: '/',
                templateUrl: '/views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm'
            })
            .state('men', {
                url: '/men',
                templateUrl: '/views/men.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('women', {
                url: '/women',
                templateUrl: '/views/women.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('accessories', {
                url: '/accessories',
                templateUrl: '/views/accessories.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('footwear', {
                url: '/footwear',
                templateUrl: '/views/footwear.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('jewelery', {
                url: '/jewelery',
                templateUrl: '/views/jewelery.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('new', {
                url: '/new',
                templateUrl: '/views/new.html',
                controller: 'menCtrl',
                controllerAs: 'vm'
            })
            .state('gift', {
                url: '/gift',
                templateUrl: '/views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm'
            })
            .state('sale', {
                url: '/sale',
                templateUrl: '/views/sale.html',
                controller: 'saleCtrl',
                controllerAs: 'vm'
            })
            .state('admin', {
                url: '/admin',
                templateUrl: '/views/admin.html',
                controller: 'AdminCtrl',
                controllerAs: 'vm'
            })
            .state('cabinet', {
                url: '/cabinet/:id',
                templateUrl: '/views/cabinet.html',
                controller: 'CabinetCtrl',
                controllerAs: 'vm'
            })
            .state('registration', {
                url: '/registration',
                templateUrl: '/views/signup.html',
                controller: 'signUpCtrl',
                controllerAs: 'vm'
            })
            .state('add_material', {
                url: '/addmaterial',
                templateUrl: '/views/add_material.html',
                controller: 'AddMaterialCtrl',
                controllerAs: 'vm'
            })
            .state('post',{
                url:'/post/:id',
                templateUrl:'/views/post.html',
                controller:'PostCtrl',
                controllerAs:'vm'
            })
            .state('profile',{
                url:'/user/:id',
                templateUrl:'views/profile.html',
                controller:'profileCtrl',
                controllerAs:'vm'
            })
    }