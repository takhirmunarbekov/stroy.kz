var mongoose = require("mongoose");

var Material = mongoose.Schema({
    name: String,
    discription: String,
    category:String,
    type: String,
    sub_type:String,
    images: [{type : String}],
    date: { type: Date},
    count:Number,
    delivery:Boolean,
    price:Number,
    price_type:String,
    country:String,
    next_delivery:Date
});

module.exports = mongoose.model("Material", Material);