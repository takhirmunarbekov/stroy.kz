var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var Cart = new mongoose.Schema({
	date: { type: Date, default: Date.now },
    user: { type: mongoose.Schema.ObjectId, ref: "User"},
	material: { type: mongoose.Schema.ObjectId, ref: "Material"}
});


module.exports = mongoose.model('Cart', Cart);
