var mongoose = require("mongoose");

var Rating = mongoose.Schema({
    user:[{ type: mongoose.Schema.ObjectId, ref: "User"}],
    material:[{ type: mongoose.Schema.ObjectId, ref: "Material"}],
    rating: Number,
    count:Number,
    
});

module.exports = mongoose.model("Rating", Rating);