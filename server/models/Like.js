var mongoose = require("mongoose");

var Like = mongoose.Schema({
    user:[{ type: mongoose.Schema.ObjectId, ref: "User"}],
    material:[{ type: mongoose.Schema.ObjectId, ref: "Material"}],
    like: Boolean
});

module.exports = mongoose.model("Like", Like);