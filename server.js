var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var multer=require("multer");
var fs = require("fs");
var cookieParser=require('cookie-parser');
var bcrypt = require('bcryptjs');
var localStrategy = require('passport-local').Strategy;
var session = require('express-session');
var passport= require('passport');
var MongoStore = require('connect-mongo')(session);
mongoose.connect('mongodb://localhost/stroy');

var upload = multer({dest:"./public/img/content"});
var app = express();

var Material = require("./server/models/Material");
var User = require("./server/models/User");

app.use(bodyParser.json({limit : '50mb'}));
app.use(bodyParser.urlencoded({limit : '50mb', extended: true}));
app.use(express.static(path.join(__dirname, 'public'), {maxAge: 1}));
app.use(logger('dev'));
app.use(cookieParser());


app.set('port',process.env.PORT||8080);

app.use(session({
    secret: 'your secret here',
    resave:  true,
    saveUninitialized: true,
    key: 'jsessionid',
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

passport.use(new localStrategy({ usernameField: 'email' },
    function( email, password, done) {
        User.findOne({ email: email }).exec(function(err, user) {
            if (err) return done(err);
            if (!user) return done(null, false);
            user.comparePassword(password, function(err, isMatch) {
                if (err) return done(err);
                if (isMatch) return done(null, user);
                return done(null, false);
            });
        });
}));

passport.serializeUser(function(user, done) {
    console.log("serializeUser", user);
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id).exec(function(err, user) {
        console.log("deserializeUser", user);
        done(err, user);
    });
});

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
    if (req.user) {
        res.cookie('user', JSON.stringify(req.user));
    }
    next();
});

//start

//Sign Up
app.post('/api/signup', function(req, res, next){
    var user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
        company: req.body.company_name,
        company_type:req.body.company_type,
        location:req.body.company_location
    })
    user.phones.push(req.body.phone);
    user.save(function(err, user){
        if(err) return res.send(err);
            req.login(user, function(err) {
                return res.json(user);
            });
        });
});
//log in
app.post('/api/login', passport.authenticate('local'), function(req, res) {
    res.cookie('user', JSON.stringify(req.user));
    console.log("LOL"+req.user);
    res.send(req.user);
});

//log out
app.post('/api/logout', function(req, res, next) {
    req.logout();
    res.clearCookie('user');
    res.send(200);
});
//create post
app.post('/api/post', upload.single('image'), function(req, res, next){
    var post= new Material({
        name:req.body.name,
        type:req.body.type,
        cost:req.body.cost,
        company:req.body.company,
        rating:req.body.rating,
        gender:req.body.gender,
        sale:req.body.sale,
        new:req.body.new
    });
    var tempPath = req.file.path;
    var targetPath = path.resolve('public/images/content/'+post._id+'.'+req.file.originalname.split('.').slice(-1).pop());
     fs.rename(tempPath,targetPath,function(err){
        if(err) return res.status(400).end();
        post.image='/images/content/'+post._id+'.'+req.file.originalname.split('.').slice(-1).pop();
        post.save(function(err){
            if(err) return res.status(400).end();
            res.send(post);
        })
    })
});

//put posts
app.put('/api/posts',function(req,res,next){
    Post.findById(req.body._id)
        .exec(function(err,post){
            if(err) return res.status(400).end();
            
            console.log(post);

            post.name=req.body.name;
            post.type=req.body.type;
            post.cost=req.body.cost;
            post.company=req.body.company;
            post.rating=req.body.rating;
            post.gender=req.body.gender;
            post.sale=req.body.sale;
            post.new=req.body.new;

            post.save(function(err){
                if (err) return res.status(400).end();
                    res.send(200);
            });
        });
    });
//get posts
        app.get('/api/posts',function(req,res,next){
            Material.find({})
                .exec(function(err,posts){
                    if(err) return res.status(400).end();
                    res.send(posts);
                });
        });

//id
app.get('/api/post/:id',function(req,res,next){
    Material.findById({_id:req.params.id})
        .exec(function(err,post){
            if(err) return res.status(400).end();
            res.send(post);
            console.log(post);
        });
});
//get user
app.get('/api/profile/:id', function(req,res,next){
    User.findById(req.session.passport.user)
        .exec(function(err,profile){
            if(err) return console.log(err);
        res.send(profile);
    });
});
//delete Post
    app.delete("/api/delete/:id", function (req, res, next) {
        Material.remove({_id: req.params.id}, function (err) {
            if (err) return res.status(400).end();
            console.log("We deleted your post");
        })
            .then(function () {
                console.log("function THEN");
                Material.find({})
                    .exec(function (err, posts) {
                        if(err) return res.status(400).end();
                        res.send(posts);
                    })
            })
            .catch(function (err){
                console.log(err);
            });
    });
    //get users
        app.get('/api/users',function(req,res,next){
            User.find()
                .exec(function(err,users){
                    if(err) return console.log(err);
                    res.send(users);
                });
        });
    //get user id
        app.get('/api/profile/:id', function(req,res,next){
            User.findById(req.session.passport.user)
                .exec(function(err,profile){
                    if(err) return console.log(err);
                    console.log(profile);
                    res.send(profile);
                });
        });
//end
app.get('*',function(req,res,next){
res.redirect('/#'+req.originalUrl);
});

var server= app.listen(app.get('port'),function(){
console.log('Express server ' +
app.get('port'));
});